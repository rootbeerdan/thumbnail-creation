# Thumbnail Creation

Script written in Go to create thumbnails for images in a directory

# Usage

Place images in /images and create an empty directory called /thumbs

Run program in said directory

# Limitations

Only tested on Windows 11, still working on macOS compatibility.