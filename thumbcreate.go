package main

import (
	"fmt"
	"image/jpeg"
	"os"
	"path/filepath"
	"strings"

	"github.com/disintegration/imaging"
)

func main() {
	// Directory to process
	dir := "./images"

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.HasSuffix(strings.ToLower(info.Name()), ".jpg") ||
			strings.HasSuffix(strings.ToLower(info.Name()), ".jpeg") {

			thumbPath := fmt.Sprintf("./thumbs/%s", info.Name())
			if _, err := os.Stat(thumbPath); os.IsNotExist(err) {
				// Open image file
				src, err := imaging.Open(path)
				if err != nil {
					return err
				}

				// Create thumbnail
				dst := imaging.Fit(src, 128, 128, imaging.Lanczos)

				// Save the thumbnail
				out, err := os.Create(thumbPath)
				if err != nil {
					return err
				}
				defer out.Close()

				// Encode the thumbnail in JPEG format
				jpeg.Encode(out, dst, nil)
			}
		}

		return nil
	})

	if err != nil {
		fmt.Println(err)
	}
}
